import React, {Component} from 'react';
import {Modal, Text, TouchableHighlight,
   View, Alert, Button, StyleSheet} from 'react-native';

import PageTemplate from './smallComponents/pageTemplate';
import { connect } from 'react-redux';
import { RadioGroup } from 'react-native-btr';

class respModal extends Component {
  state = {
    modalVisible: this.props.modalVisible,
    mes: 'I am comming out.',
    data: [
      {
        label: 'I am comming out.',
        value: 'I am comming out.',
        checked: true
      },
      {
        label: 'Will be out in 5 minutes.',
        value: 'Will be out in 5 minutes.'
      },
      {
        label: 'Will be out in 10 minutes.',
        value: 'Will be out in 10 minutes.',
      },
 

    ]
  };

  onPress = (data) => {
    this.setState({data })

    let selectedButton = this.state.data.find(e => e.checked == true);
    
    selectedButton = selectedButton ? selectedButton.value : this.state.data[0].label;
    this.setState({mes:selectedButton})
    this.props.sendData(selectedButton)
 

  };

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  render() {
    
    return (
     
      <View>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          <View >
            <PageTemplate title={'Custom Message'}/>

            
            <View style={{backgroundColor:'white',flex:1, padding:10}} />
                    
            <View style={{padding: 20}}>
              {/* <Text>Enter a custom message below. This will overide any of your automatic settings in your profile. If you leave this empty we will use an automatic message or one of your preset message:</Text> */}
            <View style={{backgroundColor:'white',flex:1, padding:10}} />
              {/* <TextInput
                style={{ 
                    borderRadius: 10,
                    height: 40,
                    maxHeight: 80,
                    width: 350, 
                    borderColor: 'gray', 
                    borderWidth: 1, 
                    marginLeft: 10}}
                    onChangeText={mesg => {this.setState({mes: mesg}), this.props.sendData(mesg)}}
                    value={this.state.mes}
              /> */}
              <RadioGroup radioButtons={this.state.data} onPress={this.onPress} />
               <View style={{backgroundColor:'white',flex:1, padding:10}} />
              <View style={styles.btnWrapper}>
                <Button
                  title={"Send"}
                  // onPress= {this.props.closeCommentModal, this.props.Mes(this.state.mes)}
                  onPress={()=> {this.props.closeCommentModal(), this.props.Mes(this.state.mes)}}
                   
                  >
                </Button>
              </View>
              
              
            </View>
          </View>
        </Modal>

        <TouchableHighlight
          onPress={() => {
            this.setModalVisible(true);
          }}>
          <Text>Show Modal</Text>
        </TouchableHighlight>
      </View>
    );
  }
}



const mapStateToProps = (state) => {
   
  const { reducer } = state
  return { reducer }
};

const mapDispachToProps = dispatch => {
  return {
    Mes: (x) => dispatch({ type: "RES_MES", value: x})
   
  };
};

export default connect(mapStateToProps,mapDispachToProps
  )(respModal)


const styles = StyleSheet.create({

  btn : {
    borderWidth:1,
    borderColor:'rgba(0,0,0,0.2)',
    alignItems:'center',
    justifyContent:'center',
    width:35,
    height:35,
    backgroundColor:'#fff',
    borderRadius:50,
    marginTop: 150
  },
  
  btnWrapper: {
    width: '40%',
    margin: 20
  },
})
