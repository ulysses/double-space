import React, {Component} from 'react';
import { StyleSheet, Text,
     View, 
      Picker, 
      Button, 
      CheckBox,
      Platform,
      Switch,
      AsyncStorage, 
      ScrollView} from 'react-native';
// import { Ionicons } from '@expo/vector-icons';
// import { List, ListItem } from 'react-native-elements'
import { connect } from 'react-redux';
// //import DateTimePicker from '@react-native-community/datetimepicker';

// import GenerateQR from './generateQR';
// import PageTemplate from './smallComponents/pageTemplate';
// import MessagingModal from './smallComponents/messagingModal';
import Times from './smallComponents/times';

// import pic from '../assets/bigRate.png';

const STORAGE_KEY = '@save_enableauto';
const DBLTIME_KEY = '@save_dbltime';

class messaging extends Component {

    state={
        modalVisible: false,
        dblParkMessage:null,
        genParkMessage:null,
        insParkMessage:null,
        messageData :{
            dblParkMessage:'I am sorry I double parked, I will respond to your message and get back to you',
            genParkMessage:'I am not sure how long I will be here',
            insParkMessage:'my insurance information is . . . '
        },
        times: Times,
        messageTimes: {
            dblTime: '12:00 pm',
            genTime: '12:00 pm'
        },
        finalMessageData:{
            dblParkMessage:null,
            genParkMessage:null
        },
        enableAuto:false
    }


    pickerChange(index, variable){
        //console.log(index)
        //this.setState({ [variable]: index});
        this.setState(prevState => ({
            messageTimes: {                   // object that we want to update
                ...prevState.messageTimes,    // keep all other key-value pairs
                [variable]: index       // update the value of specific key
            }
        }))
       }


    //for now we run seperate functions store data, first one stores msg time
    _storeMsgData = async time => {

        console.log('setting dblPark time to: ', time);
        //send check box to state
        //this.setState({ time: x });

        //set state time to feed to picker
        //this.setState(prevState => ({
        //    messageTimes: {                   // object that we want to update
        //        ...prevState.messageTimes,    // keep all other key-value pairs
        //        dblTime : index       // update the value of specific key
        //    }
        //}))

        try {
            await AsyncStorage.setItem(DBLTIME_KEY, time)
            console.log('time set at: ', time)

        } catch (e) {
            console.log(e)
          alert('We are experiencing difficulties, try to reload the app.')
        }
      }

    //this function stores whether automatic messaging is enabled
    _storeData = async enableAuto => {
        let x = enableAuto;
        console.log('setting auto messages to: ', !x);
        //send check box to state
        this.setState({ enableAuto: x });

        try {

          if(x) {
            await AsyncStorage.setItem(STORAGE_KEY, 'true')
            alert('Your Automatic Messages have been enabled.')
          } else {
            await AsyncStorage.setItem(STORAGE_KEY, 'false')
          }

        } catch (e) {
            console.log(e)
          alert('We are experiencing difficulties, try to reload the app.')
        }
      }

    _retrieveMsgData = async () =>{
        const dblTime = await AsyncStorage.getItem(DBLTIME_KEY);
        console.log('time data is:', dblTime);
    }

    //retrieves automatic messaging status
    _retrieveData = async () => {
        try {

          //pull data from local storage
          const enableAuto = await AsyncStorage.getItem(STORAGE_KEY);
          const dblTime = await AsyncStorage.getItem(DBLTIME_KEY);

          console.log('auto messages set: ',enableAuto);
          console.log('time data is:', dblTime);

          //reset state for time if it exists in local storage
          if(dblTime !==null) {
            this.setState(prevState => ({
                messageTimes: {                   // object that we want to update
                    ...prevState.messageTimes,    // keep all other key-value pairs
                    dblTime: dblTime       // update the value of specific key
                }
            }))
          } 

          //reset state for notifications if exists in local storage 
          if (enableAuto !== null) {
            // We have data!!
            console.log('receiving from local storage: ',enableAuto);
            this.setState({ enableAuto:eval(enableAuto) });
          }
        } catch (error) {
            alert('failed to load previous settings.')
          // Error retrieving data
        }
    };

    //retreive multiple data elements with one call
    _retrieveDataGroup = async () => {

        //const items = JSON.stringify([[STORAGE_KEY], [DBLTIME_KEY]]);

        try {
            const localData = AsyncStorage.multiGet([DBLTIME_KEY], () => {
                //to do something
            });

            console.log('Group fetch: ',localData);

        } catch (error) {
            alert('failed to load previous settings.')
          // Error retrieving data
        }
    };   

    //runs to establish whether automatic messages are enabled or not 
    componentDidMount() {
        //this._retrieveDataGroup();
        this._retrieveData();
        //this._retrieveMsgData();
    }

    //switch buttons
    notificationButton () {
        if (Platform.OS == 'android') {
          return (
            <View style={{flexDirection:'row'}}>
                <CheckBox
                    value={this.state.enableAuto}
                        onValueChange={() => this._storeData(!this.state.enableAuto)}
                />
                <Text>Enable Automatic Messaging</Text>
            </View>
          )
        } else {
         return (
            <View style={{flexDirection:'row'}}>
                <Switch
                    value={this.state.enableAuto}
                        onValueChange={() => this._storeData(!this.state.enableAuto)}
                />
                <Text>Enable Automatic Messaging</Text>
            </View>
         )
        }
      }



    render() {

        //this.props.reducer.messageTimes && console.log('from reducer',this.props.reducer.messageTimes);
        //console.log(Times);

        //console.log('automatic messages are: ',this.state.enableAuto);

        return (
            <ScrollView style={{flex:1}}>
                
                {/** <PageTemplate title={'Messaging'} />*/}
                <View style={{padding:10, marginTop:'5%'}}>
                    <Text style={{fontSize:17}}>MoveIt users are able to add more messages that will be automatically be displayed when someone scans your QR code or messages you.</Text>
                </View>

                <View style={{padding:10}}>
                    <Text style={{fontSize:17}}>The more information you provide for other users, the more you make! Choose from some of the predefined messages in the categories below and win automatic points that reup each month! Click on the choices below and select the choice that best fits.</Text>
                </View>



                {/** Double Parking Message */}
                <View>
                    <Text style={styles.section}>Double Parking Away (500 points)</Text>
                </View>
                
                
                <View style={styles.item}>
                        <Text style={styles.msgText}>I will be returning at:</Text>
                        <Picker
                            style={styles.timeSelector}
                            selectedValue={this.state.messageTimes.dblTime}
                            onValueChange={(itemIndex) => this.pickerChange(itemIndex, 'dblTime')}>{
                            this.state.times.map( (time, itemIndex)=>{
                                return <Picker.Item key={itemIndex} label={time.time} value={time.time} />
                            })
                            }
                        </Picker>
                </View>

            

                {/** SET MESSAGES */}
                <View styles={{flexDirection:'row', flex: 1}}>
                    
                    <View style={styles.btnWrapper}>
                        <Button title={'Set my Messages'} onPress={() => this._storeMsgData(this.state.messageTimes.dblTime)}/>
                       
                    </View>    
                        {this.notificationButton()}
                </View>

                
            </ScrollView>
        );
    }
}


//onPress={() => this.props.sendMessages(this.state.messageData)}

const mapStateToProps = (state) => {
    //console.log('rewards:', state)
    const { reducer } = state
    return { reducer }
};

const mapDispachToProps = dispatch => {
    return {
        sendMessages: (messageTimes) => dispatch({ type: "UPDATE_MESSAGES", value: messageTimes}),
    };
};

export default connect(mapStateToProps,
    mapDispachToProps
)(messaging)



const styles = StyleSheet.create({
    item: {
        flexDirection: 'row',
        justifyContent:'space-between',
        padding:10, 
        //marginTop:'5%'   
    },
    timeSelector:{
        width: '40%',
        //backgroundColor:'yellow'
    },
    btnWrapper:{
        width: '33%',
        flexDirection: 'row'
    },
    section : {
        fontSize:20,
        marginLeft:'3%',
        marginTop: '5%',
        fontSize:17,
        fontWeight: 'bold'
      },
    msgText: {
        fontStyle: 'italic',
        fontSize: 17,
        padding:15
    }
  })



 




/**
 *  <Button title={'call Data'} onPress={() => this._retrieveData()}/>
 * <Button title={'Set my Messages'} onPress={() => this.props.sendMessages(this.state.messageTimes)}/>
 * 
 *                 {this.state.modalVisible && ( <MessagingModal 
                                              modalVisible={this.state.modalVisible}
                                              />)}





                                              

                <ListItem
                    onPress={()=>{navigate('Messaging')}}
                    title={`General Parking Away`}
                    subtitle={`Leave scanners a message to let them know when you will vacate your spot`}
                    rightElement={`1000 points`}
                />

                <ListItem
                    onPress={()=>{navigate('QR')}}
                    title={`Insurance Information`}
                    subtitle={`Leave scanners a message with contact information for your insurance provider should they hit your car`}
                    rightElement={`2000 points`}
                />








                <Picker
                            selectedValue={this.state.messageData.dblParkMessage}
                            style={{ height: 50, width: 300 }}
                            onValueChange={(itemValue) =>
                                this.setState(prevState => ({
                                    messageData: {                   // object that we want to update
                                        ...prevState.messageData,    // keep all other key-value pairs
                                        dblParkMessage: itemValue       // update the value of specific key
                                    }
                                }))
                            }
                        >
                            <Picker.Item label={`I will be back in:`} value={''}  />
                            <Picker.Item label={`I will be back in`} value={''}  />
                        </Picker>







               
                <View>
                <Text>Double Parking Away (500 points)</Text>
                    
                    <View style={styles.item}>
                    <Text>I am sorry to double park, I am returning at:</Text>
                    <Picker
                        style={styles.timeSelector}
                        selectedValue={this.state.times[0] }
                        onValueChange={(itemValue, itemIndex) => this.pickerChange(itemIndex)}>{
                        this.state.times.map( (time)=>{
                            return <Picker.Item label={time.time} value={time.time} />
                        })
                        }
                    </Picker>
                    </View>
            </View>



           

            <View>
            <Text>General Parking (1000 points)</Text>
                    
                    <View style={styles.item}>
                    <Text>I will be returning at:</Text>
                    <Picker
                        style={styles.timeSelector}
                        selectedValue={this.state.times[0] }
                        onValueChange={(itemValue, itemIndex) => this.pickerChange(itemIndex)}>{
                        this.state.times.map( (time)=>{
                            return <Picker.Item label={time.time} value={time.time} />
                        })
                        }
                    </Picker>
                    </View>
            </View>



                                    <Button title={'Store Data'} onPress={() => this._storeData(this.state.enableAuto)}/>
                        <Button title={'call Data'} onPress={() => this._retrieveData()}/>









                        //latest


                          
             <View>
             <Text style={styles.section}>General Parking (1000 points)</Text>
         </View>           
                 <View style={styles.item}>
                     <Text style ={styles.msgText}>I will be returning at:</Text>
                     <Picker
                         style={styles.timeSelector}
                         selectedValue={this.state.messageTimes.genTime }
                         onValueChange={(itemIndex) => this.pickerChange(itemIndex, 'genTime')}>{
                         this.state.times.map( (time, itemIndex)=>{
                             return <Picker.Item key={itemIndex} label={time.time} value={time.time} />
                         })
                         }
                     </Picker>
                 </View>
 * 
 */