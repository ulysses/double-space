import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, TouchableHighlight, TextInput, Button, Alert } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { List, ListItem } from 'react-native-elements';
import { connect } from 'react-redux';
import Firebase from '../constants/config';

import GenerateQR from './generateQR';
import PageTemplate from './smallComponents/pageTemplate';
import pic from '../assets/bigRate.png';


class editAccount extends Component {
  
  state ={
    data:null,
    fname:null,
    lname:null,
    email:null,
    password:null,
    updatedColor:'#ff0000'
  }


  handleUpdate = (data, type) => {
    console.log('update data: ',data, 'type: ',type);
    Alert.alert(
      'Update my Information',
      'Are you sure you want to update this information?',
      [
        {text: 'Cancel', onPress: () => console.log('Cancel Pressed!')},
        {text: 'OK', onPress: () =>pushUpdate(data)},
      ]
      )

     const pushUpdate = (data) => {
        //run logic if 
      if(type==='fname') {
        this.updateFname(data);
      } else if(type==='lname') {
        this.updateLname(data);
      } else if(type==='email') {
        this.updateUser(data);
      } else if(type==='password') {
        this.updatePassword(data);
      }
     }
  
  }

  updateFname = (data) => {
    console.log('updating my first name: ' ,data);
    Firebase.database().ref('UsersList/' + this.props.reducer.uid + '/fname').set(
      data    
    );
  }

  updateLname = (data) => {
    console.log('updating my last name: ' ,data);
    Firebase.database().ref('UsersList/' + this.props.reducer.uid + '/Lname').set(
      data    
    );
  }

  updatePassword =(data) =>{
    console.log('updating password');
  }

  updateUser =() => {
    console.log('update username');
  }

  componentDidMount() {
    //run reset of data when page is brought back in 
    this.setState({ updatedColor: '#00ff00' })
  }


    render() {
        return (
            <React.Fragment>
              {/**<PageTemplate title={'Edit Account Information'} /> */}
                


                <View style={{marginLeft: '5%', marginTop: '20%'}}>
                  <Text>Edit your account information below by updating your name, email or password</Text>
                
                
                <View style={styles.itemWrapper}>
                  <TextInput
                    value={this.state.fname} 
                    placeholder={'First Name'}
                    style={styles.input}
                    onChangeText={fname => this.setState({ fname })}
                  />
                  <View style={styles.btnWrapper}>
                    <Button title={`Update`}  onPress={()=>this.handleUpdate(this.state.fname, 'fname')}/>
                  </View>
                </View>


                <View style={styles.itemWrapper}>
                  <TextInput
                    value={this.state.lname} 
                    placeholder={'Last Name'}
                    style={styles.input}
                    onChangeText={lname => this.setState({ lname })}
                  />
                  <View style={styles.btnWrapper}>
                    <Button title={`Update`} onPress={()=>this.handleUpdate(this.state.lname, 'lname')}/>
                  </View>
                </View>
                
                
                <View style={styles.itemWrapper}>
                  <TextInput
                    value={this.state.email}
                    placeholder={'Email'}
                    style={styles.input}
                    onChangeText={email => this.setState({ email })}
                  />
                  <View style={styles.btnWrapper}>
                    <Button title={`Update`}  onPress={()=>this.handleUpdate(this.state.email, 'email')}/>
                  </View>
                </View>


                <View style={styles.itemWrapper}>
                  <TextInput
                    value={this.state.password} 
                    placeholder={'Password'}
                    style={styles.input}
                    onChangeText={password => this.setState({ password })}
                  />
                  <View style={styles.btnWrapper}>
                    <Button title={`Update`}  onPress={()=>this.handleUpdate(this.state.password, 'password')}/>
                  </View>
                </View>


                </View>
            </React.Fragment>   
        );
    }
}

const mapStateToProps = (state) => {
    
  const { reducer } = state
  return { reducer }
};

const mapDispachToProps = dispatch => {
  return {
    setToken: (x) => dispatch({ type: "CLOSE_MODAL_13", value: x})
  };
};



export default connect(mapStateToProps,
  mapDispachToProps
  )(editAccount)

const styles = StyleSheet.create({
    
    container: {
        
      textAlign: 'center', // <-- the magic
       
      flex: 1,
      flexDirection: 'column',
      alignItems: 'center',
      //justifyContent: 'center',
      backgroundColor: '#e6eded'
    },

    itemWrapper: {
      flexDirection: 'row',
      justifyContent: 'space-between',   
   },
    InputContainer: {
        
      textAlign: 'center', // <-- the magic
       
      flex: 1,
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#e6eded'
    },
    buttonContainer: {
        width: '40%',
        position: 'absolute',
    bottom:40
    },
    titleText: {
      fontSize: 25,
      fontWeight: 'bold',
      marginTop: '5%'
    },
    listedText:{
        marginTop:'3%',
      fontSize: 18
    },
    input: {
      width: 200,
      height: 44,
      padding: 10,
      borderWidth: 1,
      borderColor: 'black',
      marginBottom: 10,
    },
    btnWrapper:{
      marginRight: '15%',
      width: '18%',
      height:'30%'
    }
  
   
  });