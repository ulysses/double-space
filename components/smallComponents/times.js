//generate and array of times that will be imported into messaging component

function getDoubleDigits(str) {
    return ("00" + str).slice(-2);
  }
  
  function formatTime(h, m, is24Hr) {
    var tmp = "";
    if(is24Hr){
      tmp =" " + (Math.floor(h/12) ? "p.m." : "a.m.")
      h=h%12;
    }
    return getDoubleDigits(h) + ":" + getDoubleDigits(m) + tmp;;
  }
  
  function getTimeByInterval(interval, is24HrFormat) {
    var times = []
    for (var i = 0; i < 24; i++) {
      for (var j = 0; j < 60; j += interval) {
        times.push(formatTime(i, j, is24HrFormat))
      }
    }
    return times.slice(0);
  }


  let Times =  getTimeByInterval(10,true);
  Times = Times.map((item) => ({['time']: item}))

export default Times;