import React, {Component} from 'react';
import {Share, Button} from 'react-native';
import QRCode from 'react-native-qrcode-image';
import { connect } from 'react-redux';


class ShareExample extends Component {
  onShare = async () => {
    try {
        const uri = <QRCode value={'here is my code'}/>
      const result = await Share.share({
        message:this.props.qr,
        uri: this.props.qr
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  render() {
    return <Button onPress={this.onShare} title="Share" />;
  }
}

const mapStateToProps = (state) => {
   
    const { reducer } = state
    return { reducer }
  };
  
  
  export default connect(mapStateToProps
    )(ShareExample)

