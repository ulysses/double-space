import React, { Component } from 'react';
import { StyleSheet, Text, TextInput, View, Button, Modal } from 'react-native';
import * as firebase from 'firebase';

import PageTemplate from '../smallComponents/pageTemplate';

class forgotPassword extends Component {

    state = {
        email: null,
        modalVisible: this.props.modalVisible
    }  
    
    forgotPassword = (yourEmail) => {
        console.log(yourEmail);
        firebase.auth().sendPasswordResetEmail(yourEmail)
          .then(function (user) {
            alert('Your reset link has been sent to your email, make sure to check your spam folder if you do not see it...')
          }).catch(function (e) {
            console.log(e)
          })
          
          this.setState({ modalVisible : false })
    }

    render() {
        return (
        
            <Modal
                animationType="slide"
                transparent={false}
                visible={this.state.modalVisible}
                onRequestClose={() => {
                    Alert.alert('Modal has been closed.');
              }}>
                  <PageTemplate title={"Forgot My Password"} />

              <View style={{flex:1}} > 
                
                  
                  
                  <Text style={{padding: 10, fontSize: 17}}>
                    Enter your email below to recover your password:
                  </Text>

                  <View style={{alignItems:'center'}}>
                  <TextInput
                    //secureTextEntry
                    style={styles.input}
                    autoCapitalize="none"
                    placeholder="Email"
                    onChangeText={email => this.setState({ email })}
                    value={this.state.email}
                  />
                  </View>

                


                {/**button containers */}
                <View style={styles.btnWrapper} >
                <View style={styles.btn}>
                    <Button title ={'Cancel'}
                        onPress={()=> this.setState({ modalVisible: false})} 
                    />
                  </View>

                  <View style={styles.btn} >
                    <Button title ={'Reset'}
                        onPress={()=> this.forgotPassword(this.state.email)} 
                    />
                  </View>
                </View>



      </View>
              </Modal>

        
        
        );
    }
}

export default forgotPassword;
//onPress={this.forgotPassword(this.state.email)} 

const styles = StyleSheet.create({
    
    container: {
      textAlign: 'center', // <-- the magic
      flex: 1,
      flexDirection: 'column',
      alignItems: 'center',
      //justifyContent: 'center',
      backgroundColor: '#e6eded'
    },

    texts:{
      marginLeft: '5%'
    },

    input: {
      width: 250,
      height: 44,
      padding: 10,
      borderWidth: 1.7,
      borderColor: 'black',
      marginBottom: 10
    },
  
    inputContainer: {     
      textAlign: 'center', // <-- the magic
      marginLeft:'10%'
    },
    btnWrapper:{
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-around'
    },
    btn: {
      width: '20%',
      height: 40
    }
  
   
  });


  /**
   * 
   * style={styles.inputContainer}
   * 
   */